import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import LoginForm from './Pages/LoginPage';
import BookListPage from './Pages/BookListPage';
import BookDetailsPage from './Pages/BooksDetailPage';
import Navbar from './Components/Navbar';
import CounterPage from './Pages/CounterPage'

function App() {
  return (
    <BrowserRouter>
      <div className="App">
        <Navbar />
        <Routes>
          <Route path='/' element={<LoginForm />} />
          <Route path='/login' element={<LoginForm />} />
          <Route path='/books' element={<BookListPage />} />
          <Route path='/books/:bookId' element={<BookDetailsPage />} />
          <Route path='/counter' element={<CounterPage />} />
          <Route path='*' element={ <p>You're lost! Navigate back home</p> } />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
