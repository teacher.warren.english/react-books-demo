import { useEffect, useState } from "react"
import BookListItem from "./BookListItem"
import { BOOKS_API_URL } from "../utils"
import Rating from "./Rating"

function BookDetail({ bookId }) {

    const [currentBook, setCurrentBook] = useState(null)

    useEffect(() => {
        getBookData()
    }, []) // run once

    const getBookData = async () => {
        const resp = await fetch(`${BOOKS_API_URL}/${bookId}`)
        const json = await resp.json()
        setCurrentBook(json)
    }

    const renderStars = () => {
        let stars = []
        if (currentBook){
            for (let i = 0; i < currentBook.rating; i++)
                stars.push(<Rating isFilled={ true } />)
            for (let i = 5; i > currentBook.rating; i--)
                stars.push(<Rating isFilled={ false } />)

        }

        return stars
    }

    return (
        <>
            {currentBook && <div>
                <BookListItem currentBook={currentBook} />
                <small>ISBN: {currentBook.isbn}</small>
                <p>{currentBook.blurb}</p>
                <span>Rating: </span>{ currentBook && renderStars() }
            </div>}
        </>
    )
}

export default BookDetail