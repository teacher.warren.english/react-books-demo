import { useEffect, useState } from "react"
import { BOOKS_API_URL } from "../utils"
import BookListItem from "./BookListItem"

function BookList() {

    const [books, setBooks] = useState([])

    // api call to get books and render them as BookListItem components
    useEffect(() => {
        fetch(BOOKS_API_URL)
            .then(resp => resp.json())
            .then(json => setBooks(json))
            .catch(error => console.error(error.message))
    }, []) // run once

    return (
        <>
            { books.map(book => <BookListItem currentBook={ book } key={ book.id } /> )}
        </>
    )
}

export default BookList