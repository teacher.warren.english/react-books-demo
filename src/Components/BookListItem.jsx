import { NavLink } from "react-router-dom"
import { BOOKS_API_URL_IMG } from "../utils"

function BookListItem({ currentBook: { id, title, author, coverImg } }) {

    return (
        <>
            <NavLink to={`/books/${id}`}>
                <img src={`${BOOKS_API_URL_IMG}/${coverImg}`} alt={title} width={120} />
                <h3>{`${author} - ${title}`}</h3>
            </NavLink>
            <hr />
        </>
    )
}

export default BookListItem