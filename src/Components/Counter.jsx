import { useDispatch, useSelector } from "react-redux"
import { decrement, increment, incrementByFive, incrementByNumber } from "../Redux/counterSlice"
import { useState } from "react"

const Counter = () => {

    // Hooks
    const dispatch = useDispatch()
    const counter = useSelector(state => state.counterReducer.value)
    const isNumPositive = useSelector(state => state.counterReducer.value > -1)

    const [boostValue, setBoostValue] = useState(0)


    const handleIncrement = () => {
        dispatch(increment())
    }
    const handleDecrement = () => {
        dispatch(decrement())
    }
    const handleIncrementByFive = () => {
        dispatch(incrementByFive())
    }
    const handleIncrementByNumber = () => {
        dispatch(incrementByNumber(boostValue))
    }

    const handleOnNumChange = (event) => {
        //validation of input
        if (!event.target.value)
            return

        setBoostValue(event.target.value)
    }

    return (
        <>
            <h3>Counter: {counter}</h3>
            <label>Number: <input type="number" onChange={handleOnNumChange} /></label>
            <button onClick={handleIncrement}>Increment</button>
            <button onClick={handleDecrement}>Decrement</button>
            <button onClick={handleIncrementByFive}>Boost!</button>
            <button onClick={handleIncrementByNumber}>Inc by num!</button>
            {isNumPositive && <h4>Number is positive!</h4>}
        </>
    )
}

export default Counter