import { useContext, useEffect, useState } from "react"
import { UserContext } from "../Context/UserContext"
import { useNavigate } from "react-router-dom"

function LoginForm() {
    // Hooks
    const [error, setError] = useState(null)
    const [inputText, setInputText] = useState('')

    const [user, setUser] = useContext(UserContext)

    const navigate = useNavigate()

    // redirect user to /books if they log in successfully
    useEffect(() => {
        if (user !== '')
            navigate('/books')
    }, [user]) // depend on user state

    // Handlers
    const handleLogin = () => {
        // manages the user logging in
        setUser(inputText)
    }

    const handleInputChange = (event) => {
        const inputString = event.target.value
        setError(null)

        if (inputString.length < 3) {
            setError('Username should be at least 3 characters!')
            return
        }

        setInputText(inputString)
    }

    return (
        <div>
            Username: <input type="text" onChange={ handleInputChange } />
            <button onClick={ handleLogin } disabled={error} >Login 👤</button><br />
            {error && <small>{ error }</small>}
        </div>
    )
}

export default LoginForm