import './Rating.css'

function Rating({ isFilled }) {

    return (
        <>
            { isFilled ?
                <span class="material-icons filled-star">star</span> :
                <span class="material-icons outlined-star">star</span> }
        </>
    )
}

export default Rating