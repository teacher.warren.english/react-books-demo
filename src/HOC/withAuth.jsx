import { useContext } from "react"
import { UserContext } from "../Context/UserContext"
import { Navigate } from "react-router-dom"

const withAuth = Component => props => {
    
    const [user, ] = useContext(UserContext)

    if (user !== '')
        return <Component {...props} />
    else
        return <Navigate to='/' />
}

export default withAuth