import BookList from "../Components/BookList"
import withAuth from "../HOC/withAuth"

function BookListPage() {

    return (
        <BookList />
    )
}

export default withAuth(BookListPage)