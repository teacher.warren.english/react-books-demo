import { useParams } from "react-router-dom"
import BookDetail from "../Components/BookDetail"

function BooksDetailPage() {
    // Hooks
    const { bookId } = useParams()

    return (
        <BookDetail bookId={ bookId } />
    )
}

export default BooksDetailPage