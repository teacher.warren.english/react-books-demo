import { createSlice } from "@reduxjs/toolkit";

const counterSlice = createSlice({
    name: 'counter',
    initialState: { value: 0 },
    reducers: {
        increment: state => {
            state.value += 1
        },
        decrement: state => {
            state.value -= 1
        },
        incrementByFive: state => {
            state.value += 5
        },
        incrementByNumber: (state, action) => {
            state.value += Number(action.payload)
        }
    }
})

export const { increment, decrement, incrementByFive, incrementByNumber } = counterSlice.actions
export default counterSlice.reducer